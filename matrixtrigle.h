#ifndef MATRIXTRIGLE_H
#define MATRIXTRIGLE_H
#include <iostream>
#include <vector>

template <typename T>
class Matrixtrigle
{
public:
    int n;
    T* value;
    Matrixtrigle(int _n)
    {
        n = _n;
        value = new T[n*(n+1)/2];
    }
    ~Matrixtrigle()
    {
        delete [] value;
    }
    int GetAddr(int i, int j)
    {
        return ((i >= j) ? (j + i * (i + 1) / 2) : (i + j * (j + 1) / 2));
    }
    Matrixtrigle Multiplication(Matrixtrigle & _matrix)
    {
        Matrixtrigle newmatrix(n*(n+1)/2);
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                newmatrix.value[newmatrix.GetAddr(i, j)] += this->value[this->GetAddr(i, j)] * _matrix.value[_matrix.GetAddr(i, j)];
            }
        }
        return newmatrix;
    }
};


#endif // MATRIXTRIGLE_H
