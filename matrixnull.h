#ifndef MATRIXNULL_H
#define MATRIXNULL_H
#include <iostream>
#include <vector>
using namespace std;

class Matrixnull {

    int n; // Размер матрицы (N x N)
    int not_null; // Кол-во ненулевых элементов
    int* value; // Массив значений (размер NZ)
    int* colls; // Массив номеров столбцов (размер NZ)
    int* row_index; // Массив индексов строк (размер N + 1)

public:
    Matrixnull(int _n, int _not_null) {
        n = _n;
        not_null = _not_null;
        value = new int[not_null];
        colls = new int[not_null];
        row_index = new int[n + 1];
    }

    void Multiplicate(vector<int> _vec)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < not_null; j++)
            {
                if (row_index[j] == i) value[j] *= _vec[i];
            }
        }
    }
};

#endif // MATRIXNULL_H
